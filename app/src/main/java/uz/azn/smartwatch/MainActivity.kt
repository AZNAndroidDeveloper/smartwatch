package uz.azn.smartwatch

import android.content.Context
import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import android.support.wearable.view.CardFragment
import android.support.wearable.view.WearableListView
import android.text.method.ScrollingMovementMethod
import android.view.Gravity
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.customlayout.*
import kotlinx.android.synthetic.main.customlayout2.*

class MainActivity : WearableActivity() {
    private val rowAdapter  = Adapter()
    private var counter=0
    val items  = listOf<String>("Oreo", "Naugat", "Marshmellow","Flutter","Switch","Dart")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        watch_view_stub.setOnLayoutInflatedListener {
            stub ->
            val btnTap = stub.findViewById<Button>(R.id.btn_tab)
            val btnReset = stub.findViewById<Button>(R.id.btn_reset)
            val tvCount = stub.findViewById<TextView>(R.id.tv_count)
            btnTap.setOnClickListener {
                counter++
                tvCount.text = (counter).toString()
            }
            btnReset.setOnClickListener {
                counter =0
                tvCount.text = (counter).toString()

            }

        }
//        setContentView(R.layout.customlayout2)
//        rowAdapter.setData(items)
//        wearable_list.adapter = rowAdapter

//        setContentView(R.layout.customlayout)
//        card_scroll_view.cardGravity = Gravity.BOTTOM
//        tv_scrollable.movementMethod = ScrollingMovementMethod()

//

        // Enables Always-on
        setAmbientEnabled()

    }
}