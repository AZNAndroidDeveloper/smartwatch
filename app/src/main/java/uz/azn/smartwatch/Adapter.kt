package uz.azn.smartwatch

import android.content.Context
import android.support.wearable.view.WearableListView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.gms.wearable.Wearable
import uz.azn.smartwatch.databinding.ListItemBinding

class Adapter( ): WearableListView.Adapter(){
   private val elements   = mutableListOf<String>()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WearableListView.ViewHolder {
        return ItemViewHolder(ListItemBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun getItemCount(): Int =
        elements.size

    override fun onBindViewHolder(holder: WearableListView.ViewHolder, position: Int) {
        val itemViewHolder = holder as ItemViewHolder
        holder.onBind(elements[position])
    }

    inner class ItemViewHolder(private val binding: ListItemBinding):WearableListView.ViewHolder(binding.root){

        fun onBind(element:String){
            binding.tvItem.text = element

        }
    }
    fun setData(element:List<String>){
        elements.apply { clear(); addAll(element) }
    }

}